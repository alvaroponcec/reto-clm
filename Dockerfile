FROM node:12-alpine3.14
RUN adduser node_user --disabled-password --gecos ''
USER node_user
WORKDIR /app/reto-clm
COPY package.json ./
RUN npm install
#RUN npm ci --only=production && npm cache clean --force
COPY . .
COPY --chown=node_user:node_user . .
EXPOSE 3000
CMD node index.js