	sudo apt update
	sudo apt install git
	sudo apt install apt-transport-https ca-certificates curl software-properties-common
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
	sudo apt update
	sudo apt install docker-ce
	sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-Linux-x86_64" -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
	sudo curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.20.0/bin/linux/amd64/kubectl
	sudo chmod +x kubectl
	sudo mv ./kubectl /usr/local/bin/kubectl
	sudo curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
	sudo chmod +x minikube
	sudo mkdir -p /usr/local/bin/
	sudo install minikube /usr/local/bin/
	sudo docker system prune -a
	sudo docker build . -t reto-clm/w1
	sudo docker run --name reto-clm -d -p 3000:3000 reto-clm/w1
	sudo docker rm -f reto-clm
	sudo docker rmi -f reto-clm/w1
	sudo docker-compose build
	sudo docker-compose up -d
	sudo docker-compose stop
	sudo docker system prune -a

	



